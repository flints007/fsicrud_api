package com.crud.util;

import com.crud.model.Dispenser;
import com.crud.model.Tank;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PayloadValidatorTest {


        
        @Test
	public void validateTankPayLoad() {
		Tank tank = new Tank(1L, "TANK001", "J007", "GAS", 456, new Dispenser(5L, "MGP009", "MDf", "KERO", "Manual", "Standard", "Local"));
		assertEquals(false, PayloadValidator.validateCreateTankPayload(tank));
	}
        
        @Test
	public void validateInvalidTankPayLoad() {
		Tank tank = new Tank(-0, "TANK001", "J007", "GAS", 456, new Dispenser(0, "MGP009", "MDf", "KERO", "Manual", "Standard", "Local"));
		assertEquals(true, PayloadValidator.validateCreateTankPayload(tank));
	}
        
	 @Test
	public void validateDispenserPayLoad() {
		Dispenser dispenser = new Dispenser(1L, "MGP009", "MDf", "KERO", "Manual", "Standard", "Local");
		assertEquals(false, PayloadValidator.validateCreateDispenserPayload(dispenser));
	}
        
	 @Test
	public void validateInvalidDispenserPayLoad() {
		Dispenser dispenser = new Dispenser(0, "MGP009", "MDf", "KERO", "Manual", "Standard", "Local");
		assertEquals(true, PayloadValidator.validateCreateDispenserPayload(dispenser));
	}
	

}
