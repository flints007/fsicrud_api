/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crud.controller;

import com.crud.FsiCrudApiApplication;
import com.crud.controller.DispenserController;
import com.crud.model.Dispenser;
import com.crud.model.Response;
import java.util.List;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Terseer
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FsiCrudApiApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DispenserControllerTest {
 
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }

	@Test
	public void verifyAllDispenserList() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/dispensers").accept(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$", hasSize(4))).andDo(print());
	}
	
	@Test
	public void verifyDispenserById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/dispensers/3").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		.andExpect(jsonPath("$.hscode").exists())
		.andExpect(jsonPath("$.modelno").exists())
		.andExpect(jsonPath("$.name").value("MGP003"))
		.andExpect(jsonPath("$.hscode").value("Df03"))
		.andDo(print());
	}
	
	@Test
	public void verifyInvalidDispenserArgument() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/dispensers/f").accept(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.errorCode").value(400))
			.andExpect(jsonPath("$.message").value("The request could not be understood by the server due to malformed syntax."))
			.andDo(print());
	}
	
	@Test
	public void verifyInvalidDispenserId() throws Exception {
	mockMvc.perform(MockMvcRequestBuilders.get("/dispensers/0").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.errorCode").value(404))
		.andExpect(jsonPath("$.message").value("ToDo doesn´t exist"))
		.andDo(print());
	}
        
    
	
	@Test
	public void verifyNullDispenser() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/dispensers/6").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.errorCode").value(404))
		.andExpect(jsonPath("$.message").value("ToDo doesn´t exist"))
		.andDo(print());
	}
	
	@Test
	public void verifyDeleteDispenser() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.delete("/dispensers/4").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.status").value(200))
		.andExpect(jsonPath("$.message").value("Dispenser has been deleted"))
		.andDo(print());
	}
	
	@Test
	public void verifyInvalidDispenserIdToDelete() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.delete("/dispensers/9").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.errorCode").value(404))
		.andExpect(jsonPath("$.message").value("The request could not be understood by the server due to malformed syntax"))
		.andDo(print());
	}
        
    
	@Test
	public void verifySaveDispenser() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/dispensers")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"name\" : \"MGP0049\", \"hscode\" : \"Df04\", \"modelno\" : \"M00O4\", \"magnitudeofdischarge\" : \"Manual\", \"operationmode\" : \"Standard\", \"oiltransportationmode\" : \"Local\" }")
		.accept(MediaType.APPLICATION_JSON))
                        
		.andExpect(jsonPath("$.id").exists())
		.andExpect(jsonPath("$.name").exists())
		.andExpect(jsonPath("$.hscode").exists())
                 .andExpect(jsonPath("$.modelno").exists())
		.andExpect(jsonPath("$.magnitudeofdischarge").exists())
                .andExpect(jsonPath("$.operationmode").exists())
                .andExpect(jsonPath("$.oiltransportationmode").exists())
		.andDo(print());
	}
 
	@Test
	public void verifyMalformedSaveDispenser() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/dispensers")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{ \"id\": \"8\", \"name\" : \"MGP0049\", \"hscode\" : \"Df04\", \"modelno\" : \"M00O4\", \"magnitudeofdischarge\" : \"Manual\", \"operationmode\" : \"Standard\", \"oiltransportationmode\" : \"Local\" }")
		.accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.errorCode").value(400))
		.andExpect(jsonPath("$.message").value("Payload malformed, id must not be defined"))
		.andDo(print());
	}
//	
	@Test
	public void verifyUpdateDispenser() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.patch("/dispensers")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{ \"id\": \"1\",\"name\" : \"MGP0049\", \"hscode\" : \"Df04\", \"modelno\" : \"M00O4\", \"magnitudeofdischarge\" : \"Manual\", \"operationmode\" : \"Standard\", \"oiltransportationmode\" : \"Local\"  }")
        .accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		.andExpect(jsonPath("$.name").exists())
		.andExpect(jsonPath("$.hscode").exists())
                 .andExpect(jsonPath("$.modelno").exists())
		.andExpect(jsonPath("$.magnitudeofdischarge").exists())
                .andExpect(jsonPath("$.operationmode").exists())
                .andExpect(jsonPath("$.oiltransportationmode").exists())
		.andDo(print());
	}
//	
	@Test
	public void verifyInvalidToDoUpdate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.patch("/dispenser/")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{ \"idd\": \"8\", \"name\" : \"MGP0049\", \"hscode\" : \"Df04\", \"modelno\" : \"M00O4\", \"magnitudeofdischarge\" : \"Manual\", \"operationmode\" : \"Standard\", \"oiltransportationmode\" : \"Local\" }")
		.accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.errorCode").value(404))
		.andExpect(jsonPath("$.message").value("Dispenser to update doesn´t exist"))
		.andDo(print());
	}

    
}
