/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crud.controller;

import com.crud.FsiCrudApiApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Terseer
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FsiCrudApiApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StationTransactionControllerTest {
    
    public StationTransactionControllerTest() {
    }
    
private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }

    /**
     * Test of addStationTransaction method, of class StationTransactionController.
     * @throws java.lang.Exception
     */
 
    	@Test
	public void testAddStationTransaction() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/transactions")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"userid\" : \"MGP0049\", \"tankid\" : \"Df04\", \"volleftintank\" : \"M00O4\", \"magnitudeofdischarge\" : \"Manual\", \"operationmode\" : \"Standard\", \"oiltransportationmode\" : \"Local\" }")
		.accept(MediaType.APPLICATION_JSON))
                        
		.andExpect(jsonPath("$.id").exists())
		.andExpect(jsonPath("$.userid").exists())
		.andExpect(jsonPath("$.tankid").exists())
                 .andExpect(jsonPath("$.volleftintank").exists())
		.andExpect(jsonPath("$.volsoldbydispenser").exists())
                .andExpect(jsonPath("$.transactiontype").exists())
                .andExpect(jsonPath("$.beforedelivery").exists())
                 .andExpect(jsonPath("$.newproduct").exists())
		.andDo(print());
	}
    
}
