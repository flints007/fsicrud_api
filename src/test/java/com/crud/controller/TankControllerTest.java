/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crud.controller;

import com.crud.FsiCrudApiApplication;
import com.crud.model.Response;
import com.crud.model.Tank;
import java.util.List;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Terseer
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FsiCrudApiApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TankControllerTest {
    
    public TankControllerTest() {
    }
    
  private MockMvc mockMvc;
	
	@Autowired
    private WebApplicationContext wac;

	@Before
	public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

	}

	@Test
	public void verifyAllTankList() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/tanks").accept(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$", hasSize(4))).andDo(print());
	}
	
	@Test
	public void verifyTankById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/tanks/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		.andExpect(jsonPath("$.name").exists())
		.andExpect(jsonPath("$.tanktype").exists())
		.andExpect(jsonPath("$.storedfuel").value("GAS"))
		.andExpect(jsonPath("$.liter").exists())
		.andDo(print());
	}
 
	@Test
	public void verifyInvalidTankArgument() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/tanks/f").accept(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.errorCode").value(400))
			.andExpect(jsonPath("$.message").value("The request could not be understood by the server due to malformed syntax."))
			.andDo(print());
	}
	
	@Test
	public void verifyInvalidTankId() throws Exception {
	mockMvc.perform(MockMvcRequestBuilders.get("/tanks/0").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.errorCode").value(404))
		.andExpect(jsonPath("$.message").value("Tank doesn´t exist"))
		.andDo(print());
	}
 
	@Test
	public void verifyNullTank() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/tanks/6").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.errorCode").value(404))
		.andExpect(jsonPath("$.message").value("Tank doesn´t exist"))
		.andDo(print());
	}
	
	@Test
	public void verifyDeleteTank() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.delete("/tanks/4").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.status").value(200))
		.andExpect(jsonPath("$.message").value("Tank has been deleted"))
		.andDo(print());
	}
	
	@Test
	public void verifyInvalidTankIdToDelete() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.delete("/tanks/9").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.errorCode").value(404))
		.andExpect(jsonPath("$.message").value("The request could not be understood by the server due to malformed syntax"))
		.andDo(print());
	}
       
    //new Tank(1L, "TANK001", "J007", "GAS", 456, new Dispenser(1L, "MGP009", "MDf", "KERO", "Manual", "Standard", "Local") private String name;
    private String hscode;
    private String model_no; 
    private String magnitudeofdischarge; 
    private String operation_mode; 
    private String oiltransportationmode;
	@Test
	public void verifySaveDispenser() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/tanks")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"name\" : \"TANK001\", \"tanktype\" : \"J007\", \"storedfuel\" : \"GAS\", \"liter\" : \"456\", \"dispenserid\" : \"{'1L, \"name\": \"MGP009\", \"hscode\" : \"MDf \", \"modelno\" : \"MDf00 \" , \"magnitudeofdischarge\" :\"Manual\" ,\"operationmode\" : \"Standard\", \"oiltransportationmode\": \"Local'}\"  }")
		.accept(MediaType.APPLICATION_JSON))
                        
		.andExpect(jsonPath("$.id").exists())
		.andExpect(jsonPath("$.tanktype").exists())
		.andExpect(jsonPath("$.storedfuel").exists())
                 .andExpect(jsonPath("$.liter").exists())
		.andExpect(jsonPath("$.dispenserid").exists()) 
		.andDo(print());
	}
 

    
}
