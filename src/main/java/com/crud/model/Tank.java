/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crud.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Terseer
 */
@Entity
public class Tank {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String tank_type;
    private String stored_fuel;
    private int liter;

    @OneToOne
    private Dispenser dispenserid;

    @Column(name = "created_at")
    private LocalDateTime createDateTime;

    @Column(name = "updated_at")
    private LocalDateTime updateDateTime;

    public Tank() {
        super();
    }

    public Tank(long id, String name, String tanktype, String storedfuel, int liter, Dispenser dispenserid) {
        this.id = id;
        this.name = name;
        this.tank_type = tanktype;
        this.stored_fuel = storedfuel;
        this.liter = liter;
        this.dispenserid = dispenserid;
    }

    public Tank(String name, String tanktype, String storedfuel, int liter, Dispenser dispenserid) {
        this.name = name;
        this.tank_type = tanktype;
        this.stored_fuel = storedfuel;
        this.liter = liter;
        this.dispenserid = dispenserid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTanktype() {
        return tank_type;
    }

    public void setTanktype(String tanktype) {
        this.tank_type = tanktype;
    }

    public String getStoredfuel() {
        return stored_fuel;
    }

    public void setStoredfuel(String storedfuel) {
        this.stored_fuel = storedfuel;
    }

    public int getLiter() {
        return liter;
    }

    public void setLiter(int liter) {
        this.liter = liter;
    }

    public Dispenser getDispenserid() {
        return dispenserid;
    }

    public void setDispenserid(Dispenser dispenserid) {
        this.dispenserid = dispenserid;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    @Override
    public String toString() {
        return "com.fsi_crud_api.entity.Tank[ id=" + id + " ]";
    }

}
