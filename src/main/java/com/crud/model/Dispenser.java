/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Terseer
 */
@Entity
public class Dispenser {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String hscode;
    private String model_no;
    private String magnitude_of_discharge;
    private String operation_mode;
    private String oil_transportation_mode;
    
    public Dispenser() {
            super();
    }

    public Dispenser(long id, String name, String hscode, String modelno, String magnitudeofdischarge, String operationmode, String oiltransportationmode) {
        super();
        this.id = id;
        this.name = name;
        this.hscode = hscode;
        this.model_no = modelno;
        this.magnitude_of_discharge = magnitudeofdischarge;
        this.operation_mode = operationmode;
        this.oil_transportation_mode = oiltransportationmode;
    }

    public Dispenser(String name, String hscode, String modelno, String magnitudeofdischarge, String operationmode, String oiltransportationmode) {
         super();
        this.name = name;
        this.hscode = hscode;
        this.model_no = modelno;
        this.magnitude_of_discharge = magnitudeofdischarge;
        this.operation_mode = operationmode;
        this.oil_transportation_mode = oiltransportationmode;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHscode() {
        return hscode;
    }

    public void setHscode(String hscode) {
        this.hscode = hscode;
    }

    public String getModelno() {
        return model_no;
    }

    public void setModelno(String modelno) {
        this.model_no = modelno;
    }

    public String getMagnitudeofdischarge() {
        return magnitude_of_discharge;
    }

    public void setMagnitudeofdischarge(String magnitudeofdischarge) {
        this.magnitude_of_discharge = magnitudeofdischarge;
    }

    public String getOperationmode() {
        return operation_mode;
    }

    public void setOperationmode(String operationmode) {
        this.operation_mode = operationmode;
    }

    public String getOiltransportationmode() {
        return oil_transportation_mode;
    }

    public void setOiltransportationmode(String oiltransportationmode) {
        this.oil_transportation_mode = oiltransportationmode;
    }

//    @Override
//    public String toString() {
//        return "com.fsi_crud_api.entity.Dispenser[ id=" + id + " ]";
//    }

}
