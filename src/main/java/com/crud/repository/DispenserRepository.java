/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crud.repository;

import com.crud.model.Dispenser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Terseer
 */
@Repository("dispenserRepository")
public interface DispenserRepository extends JpaRepository<Dispenser, Long> {

    public Dispenser findOne(long dispenserId);

}
