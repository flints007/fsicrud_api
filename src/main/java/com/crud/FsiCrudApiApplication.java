package com.crud;

import com.crud.model.Dispenser;
import com.crud.model.Tank;
import com.crud.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.crud.repository.DispenserRepository;
import com.crud.repository.TankRepository;
import com.crud.repository.UserRepository;

@SpringBootApplication
public class FsiCrudApiApplication {

    private static final Logger logger = LoggerFactory.getLogger(FsiCrudApiApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(FsiCrudApiApplication.class, args);
    }

    @Bean
    public CommandLineRunner setup(UserRepository userRepository, TankRepository tankRepository, DispenserRepository dispenserRepository) {
        return (args) -> {
            
            userRepository.save(new User("Terseer", "agbe.terseer@gmail.com", "secrate"));
            
            logger.info("Adding sample data for Dispenser");
            dispenserRepository.save(new Dispenser(1L, "MGP009", "Df09", "M0001", "Manual", "Standard", "Local"));
            dispenserRepository.save(new Dispenser(2L, "MGP002", "Df02", "M00O2", "Manual", "Standard", "Local"));
            dispenserRepository.save(new Dispenser(3L, "MGP003", "Df03", "M00O3", "Manual", "Standard", "Local"));
            dispenserRepository.save(new Dispenser(4L, "MGP004", "Df04", "M00O4", "Manual", "Standard", "Local"));

            logger.info("Adding sample data for Tank");
            tankRepository.save(new Tank(1L, "TANK001", "J007", "GAS", 456, new Dispenser(1L, "MGP009", "MDf", "KERO", "Manual", "Standard", "Local")));
            tankRepository.save(new Tank(2L, "TANK002", "J004", "GAS", 416, new Dispenser(2L, "MGP009", "MDf", "KERO", "Manual", "Standard", "Local")));
            tankRepository.save(new Tank(3L, "TANK003", "J003", "GAS", 436, new Dispenser(3L, "MGP009", "MDf", "KERO", "Manual", "Standard", "Local")));
            tankRepository.save(new Tank(4L, "TANK004", "J008", "GAS", 456, new Dispenser(4L, "MGP009", "MDf", "KERO", "Manual", "Standard", "Local")));


            logger.info("The sample data has been generated");
        };
    }
}
