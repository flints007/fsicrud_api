package com.crud.util;

import com.crud.model.Dispenser;
import com.crud.model.StationTransaction;
import com.crud.model.Tank;

public class PayloadValidator {

    public static boolean validateCreateTankPayload(Tank tank) {
        return tank.getId() <= 0;
    }

    public static boolean validateCreateDispenserPayload(Dispenser dispenser) {
        return dispenser.getId() <= 0;
    }

    public static boolean validateCreateStationTransactionPayload(StationTransaction dispenser) {

        return dispenser.getId() <= 0;
    }

}
