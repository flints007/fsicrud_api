-------------------------------------------------------------------------------
Test set: com.crud.controller.ToDoControllerTest
-------------------------------------------------------------------------------
Tests run: 11, Failures: 9, Errors: 0, Skipped: 0, Time elapsed: 9.202 sec <<< FAILURE! - in com.crud.controller.ToDoControllerTest
verifyAllToDoList(com.crud.controller.ToDoControllerTest)  Time elapsed: 0.515 sec  <<< FAILURE!
java.lang.AssertionError: JSON path "$"
Expected: a collection with size <4>
     but: collection size was <0>
	at org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:74)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers$1.match(JsonPathResultMatchers.java:86)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:171)
	at com.crud.controller.ToDoControllerTest.verifyAllToDoList(ToDoControllerTest.java:44)

verifyDeleteToDo(com.crud.controller.ToDoControllerTest)  Time elapsed: 0.094 sec  <<< FAILURE!
java.lang.AssertionError: No value at JSON path "$.status", exception: No results for path: $['status']
	at org.springframework.test.util.JsonPathExpectationsHelper.evaluateJsonPath(JsonPathExpectationsHelper.java:245)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:99)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers$2.match(JsonPathResultMatchers.java:99)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:171)
	at com.crud.controller.ToDoControllerTest.verifyDeleteToDo(ToDoControllerTest.java:86)

verifyInvalidToDoArgument(com.crud.controller.ToDoControllerTest)  Time elapsed: 0.016 sec  <<< FAILURE!
java.lang.AssertionError: JSON path "$.errorCode" expected:<400> but was:<0>
	at org.springframework.test.util.AssertionErrors.fail(AssertionErrors.java:54)
	at org.springframework.test.util.AssertionErrors.assertEquals(AssertionErrors.java:81)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:116)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers$2.match(JsonPathResultMatchers.java:99)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:171)
	at com.crud.controller.ToDoControllerTest.verifyInvalidToDoArgument(ToDoControllerTest.java:62)

verifyInvalidToDoId(com.crud.controller.ToDoControllerTest)  Time elapsed: 0.016 sec  <<< FAILURE!
java.lang.AssertionError: JSON path "$.errorCode" expected:<404> but was:<0>
	at org.springframework.test.util.AssertionErrors.fail(AssertionErrors.java:54)
	at org.springframework.test.util.AssertionErrors.assertEquals(AssertionErrors.java:81)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:116)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers$2.match(JsonPathResultMatchers.java:99)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:171)
	at com.crud.controller.ToDoControllerTest.verifyInvalidToDoId(ToDoControllerTest.java:70)

verifyInvalidToDoIdToDelete(com.crud.controller.ToDoControllerTest)  Time elapsed: 0.016 sec  <<< FAILURE!
java.lang.AssertionError: JSON path "$.errorCode" expected:<404> but was:<0>
	at org.springframework.test.util.AssertionErrors.fail(AssertionErrors.java:54)
	at org.springframework.test.util.AssertionErrors.assertEquals(AssertionErrors.java:81)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:116)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers$2.match(JsonPathResultMatchers.java:99)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:171)
	at com.crud.controller.ToDoControllerTest.verifyInvalidToDoIdToDelete(ToDoControllerTest.java:94)

verifyInvalidToDoUpdate(com.crud.controller.ToDoControllerTest)  Time elapsed: 0.062 sec  <<< FAILURE!
java.lang.AssertionError: JSON path "$.errorCode" expected:<404> but was:<0>
	at org.springframework.test.util.AssertionErrors.fail(AssertionErrors.java:54)
	at org.springframework.test.util.AssertionErrors.assertEquals(AssertionErrors.java:81)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:116)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers$2.match(JsonPathResultMatchers.java:99)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:171)
	at com.crud.controller.ToDoControllerTest.verifyInvalidToDoUpdate(ToDoControllerTest.java:146)

verifyMalformedSaveToDo(com.crud.controller.ToDoControllerTest)  Time elapsed: 0.031 sec  <<< FAILURE!
java.lang.AssertionError: JSON path "$.errorCode" expected:<404> but was:<0>
	at org.springframework.test.util.AssertionErrors.fail(AssertionErrors.java:54)
	at org.springframework.test.util.AssertionErrors.assertEquals(AssertionErrors.java:81)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:116)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers$2.match(JsonPathResultMatchers.java:99)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:171)
	at com.crud.controller.ToDoControllerTest.verifyMalformedSaveToDo(ToDoControllerTest.java:120)

verifyNullToDo(com.crud.controller.ToDoControllerTest)  Time elapsed: 0.016 sec  <<< FAILURE!
java.lang.AssertionError: JSON path "$.errorCode" expected:<404> but was:<0>
	at org.springframework.test.util.AssertionErrors.fail(AssertionErrors.java:54)
	at org.springframework.test.util.AssertionErrors.assertEquals(AssertionErrors.java:81)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:116)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers$2.match(JsonPathResultMatchers.java:99)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:171)
	at com.crud.controller.ToDoControllerTest.verifyNullToDo(ToDoControllerTest.java:78)

verifyToDoById(com.crud.controller.ToDoControllerTest)  Time elapsed: 0.016 sec  <<< FAILURE!
java.lang.AssertionError: No value at JSON path "$.id", exception: No results for path: $['id']
	at org.springframework.test.util.JsonPathExpectationsHelper.evaluateJsonPath(JsonPathExpectationsHelper.java:245)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertExistsAndReturn(JsonPathExpectationsHelper.java:260)
	at org.springframework.test.util.JsonPathExpectationsHelper.exists(JsonPathExpectationsHelper.java:182)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers$3.match(JsonPathResultMatchers.java:116)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:171)
	at com.crud.controller.ToDoControllerTest.verifyToDoById(ToDoControllerTest.java:50)

